local S
if (minetest.get_modpath("intllib")) then
	S = intllib.Getter()
else
	S = function ( s ) return s end
end

minetest.register_node("pseudonodes:pseudo_block", {
	description = S("Pseudo-block"),
	drawtype = "glasslike_framed",
	paramtype = "light",
	tiles = { "pseudonodes_pseudo_block.png", "pseudonodes_nothing.png" },
	inventory_image = minetest.inventorycube("pseudonodes_pseudo_block.png"),
	groups = { pseudo = 1 },
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	sounds = { dig = { name="", gain = 0 } },
	alpha = 0,
	stack_max = 9999,
	drop = "",
	on_blast = function() end,
})
minetest.register_node("pseudonodes:replacable_pseudo_block", {
	description = S("Replacable pseudo-block"),
	drawtype = "glasslike_framed",
	paramtype = "light",
	tiles = { "pseudonodes_replacable_pseudo_block.png", "pseudonodes_nothing.png" },
	inventory_image = minetest.inventorycube("pseudonodes_replacable_pseudo_block.png"),
	groups = { pseudo = 1 },
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	sounds = { dig = { name="", gain = 0 } },
	alpha = 0,
	buildable_to = true,
	stack_max = 9999,
	drop = "",
	on_blast = function() end,
})

minetest.register_node("pseudonodes:replacable_block", {
	description = S("Replacable block"),
	tiles = { "pseudonodes_replacable_block.png" },
	groups = { pseudo = 1 },
	is_ground_content = false,
	walkable = true,
	buildable_to = true,
	sounds = { dig = { name="", gain = 0 } },
	stack_max = 9999,
	drop = "",
	on_blast = function() end,
})

minetest.register_node("pseudonodes:pseudo_block_timer", {
	description = S("Timed pseudo-block"),
	drawtype = "glasslike_framed",
	paramtype = "light",
	tiles = {
		{
			image="pseudonodes_pseudo_block_timer_animated.png",
			animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=2}
		},
		"pseudonodes_nothing.png",
	},
	inventory_image = minetest.inventorycube("pseudonodes_pseudo_block_timer.png"),
	groups = { pseudo = 1 },
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	sounds = { dig = { name="", gain = 0 } },
	alpha = 0,
	stack_max = 9999,
	drop = "",
	on_construct = function(pos)
		local timer = minetest.get_node_timer(pos)
		timer:start(5)
	end,
	on_timer = function(pos,elapsed)
		minetest.remove_node(pos)
	end,
	on_blast = function() end,
})

minetest.register_node("pseudonodes:replacable_pseudo_block_timer", {
	description = S("Replacable timed pseudo-block"),
	drawtype = "glasslike_framed",
	paramtype = "light",
	tiles = {
		{
			image="pseudonodes_replacable_pseudo_block_timer_animated.png",
			animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=2}
		},
		"pseudonodes_nothing.png",
	},
	inventory_image = minetest.inventorycube("pseudonodes_replacable_pseudo_block_timer.png"),
	groups = { pseudo = 1 },
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	sounds = { dig = { name="", gain = 0 } },
	alpha = 0,
	buildable_to = true,
	stack_max = 9999,
	drop = "",
	on_construct = function(pos)
		local timer = minetest.get_node_timer(pos)
		timer:start(5)
	end,
	on_timer = function(pos,elapsed)
		minetest.remove_node(pos)
	end,
	on_blast = function() end,
})

minetest.register_node("pseudonodes:switch_block_off", {
	description = S("Switch block (off)"),
	drawtype = "glasslike",
	paramtype = "light",
	tiles = { "pseudonodes_switch_block_off.png" },
	inventory_image = minetest.inventorycube("pseudonodes_switch_block_off.png"),
	sunlight_propagates = true,
	walkable = false,
	sounds = { dig = { name="", gain = 0 } },
	groups = { pseudo = 1 },
	is_ground_content = false,
	alpha = 0,
	stack_max = 9999,
	drop = "",
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		local newnode = node
		newnode.name = "pseudonodes:switch_block_on"
		minetest.swap_node(pos, newnode)
		return itemstack
	end,
	on_blast = function() end,
})

minetest.register_node("pseudonodes:switch_block_on", {
	description = S("Switch block (on)"),
	tiles = { "pseudonodes_switch_block_on.png" },
	inventory_image = minetest.inventorycube("pseudonodes_switch_block_on.png"),
	groups = { pseudo = 1 },
	is_ground_content = false,
	diggable = true,
	walkable = true,
	stack_max = 9999,
	drop = "",
	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		local newnode = node
		newnode.name = "pseudonodes:switch_block_off"
		minetest.swap_node(pos, newnode)
		return itemstack
	end,
	on_blast = function(pos, intensity)
		minetest.set_node(pos, {name="pseudonodes:switch_block_off"})
	end,
})


minetest.register_tool("pseudonodes:pick", {
	description = S("Pseudo-pick"),
	inventory_image = "pseudonodes_pick.png",
	tool_capabilities = {
		groupcaps = { pseudo = { times = {[1]=0}, maxlevel=1, maxwear=0 }},
	},
})
